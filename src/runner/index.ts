/** Imports */
import "./conditions/compare";
import "./conditions/match";
import "./conditions/score";

/** Exports */
export { Scale } from "./scale";
export * from "./interface";
