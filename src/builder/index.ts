/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    NodeBlock,
    Num,
    Slots,
    affects,
    castToNumber,
    conditions,
    definition,
    each,
    editor,
    filter,
    insertVariable,
    isBoolean,
    isNumberFinite,
    isString,
    npgettext,
    pgettext,
    slots,
    supplies,
    tripetto,
} from "@tripetto/builder";
import { ScaleOption } from "./option";
import { ScaleMatchCondition } from "./conditions/match";
import { ScaleCompareCondition } from "./conditions/compare";
import { TMode } from "../runner/conditions/mode";
import { MAX, MAX_DEFAULT, MIN, MIN_DEFAULT } from "../runner/range";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_UNANSWERED from "../../assets/unanswered.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:scale", "Scale");
    },
})
export class Scale extends NodeBlock {
    get useOptions() {
        return this.mode === "options";
    }

    @definition
    @affects("#label")
    @affects("#slots")
    mode: "numeric" | "options" = "numeric";

    @definition("items")
    @affects("#label")
    @supplies<Scale>("#slot", "scale", true, "useOptions")
    readonly options = Collection.of<ScaleOption, Scale>(ScaleOption, this);

    @definition("number", "optional")
    @affects("#label")
    from?: number;

    @definition("number", "optional")
    @affects("#label")
    to?: number;

    @definition("number", "optional")
    stepSize?: number;

    @definition("string", "optional")
    labelLeft?: string;

    @definition("string", "optional")
    labelCenter?: string;

    @definition("string", "optional")
    labelRight?: string;

    @definition("boolean", "optional")
    justify?: boolean;

    scaleSlot!: Slots.Number | Slots.String;

    get label() {
        if (this.mode === "numeric") {
            const from = Num.range(
                Num.min(
                    castToNumber(this.from, MIN_DEFAULT),
                    castToNumber(this.to, MAX_DEFAULT)
                ),
                MIN,
                MAX
            );
            const to = Num.range(
                Num.max(
                    castToNumber(this.from, MIN_DEFAULT),
                    castToNumber(this.to, MAX_DEFAULT)
                ),
                MIN,
                MAX
            );

            return `${this.type.label} (${Num.format(from)}-${Num.format(to)})`;
        } else {
            return npgettext(
                "block:scale",
                "%2 (%1 option)",
                "%2 (%1 options)",
                this.options.count,
                this.type.label
            );
        }
    }

    @slots
    defineSlots(): void {
        this.scaleSlot = this.slots.static<Slots.Number | Slots.String>({
            type: this.mode === "numeric" ? Slots.Number : Slots.String,
            reference: "scale",
            label: pgettext("block:scale", "Scale"),
            ...(this.scaleSlot && {
                required: this.scaleSlot.required,
                alias: this.scaleSlot.alias,
                exportable: this.scaleSlot.exportable,
            }),
            exchange: ["required", "alias", "exportable"],
        });
    }

    @editor
    defineEditor(): void {
        if (
            isNumberFinite(this.from) &&
            isNumberFinite(this.to) &&
            this.to < this.from
        ) {
            const from = this.from;

            this.from = this.to;
            this.to = from;
        }

        const labelLeft = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "labelLeft", undefined)
        )
            .placeholder(pgettext("block:scale", "Left"))
            .action("@", insertVariable(this));
        const labelCenter = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "labelCenter", undefined)
        )
            .placeholder(pgettext("block:scale", "Center"))
            .action("@", insertVariable(this));
        const labelRight = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "labelRight", undefined)
        )
            .placeholder(pgettext("block:scale", "Right"))
            .action("@", insertVariable(this));

        const labelUpdate = () => {
            const count =
                this.mode === "options"
                    ? filter(this.options.all, (o) => (o.name ? true : false))
                          .length
                    : Math.abs(
                          castToNumber(this.from, MAX_DEFAULT) -
                              castToNumber(this.to, MIN_DEFAULT)
                      ) + 1;

            labelLeft.disabled(count === 0);
            labelCenter.disabled(count < 3);
            labelRight.disabled(count < 2);
        };

        this.editor.groups.general();
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:scale", "Mode"),
            form: {
                title: pgettext("block:scale", "Scale mode"),
                controls: [
                    new Forms.Radiobutton<"numeric" | "options">(
                        [
                            {
                                label: pgettext("block:scale", "Numeric scale"),
                                value: "numeric",
                            },
                            {
                                label: pgettext("block:scale", "Text scale"),
                                value: "options",
                            },
                        ],
                        Forms.Radiobutton.bind(this, "mode", "numeric")
                    ).on((t) => {
                        numbericScale.disabled(t.value === "options");
                        numbericScale.visible(t.value === "numeric");
                        optionsScale.disabled(t.value === "numeric");
                        optionsScale.visible(t.value === "options");
                        labelUpdate();
                    }),
                ],
            },
            locked: true,
        });

        const numbericScale = this.editor.option({
            name: pgettext("block:scale", "Scale range"),
            form: {
                title: pgettext("block:scale", "Scale range"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "from", undefined, MIN_DEFAULT)
                    )
                        .label(pgettext("block:scale", "From"))
                        .min(MIN)
                        .max(MAX)
                        .on(() => labelUpdate()),
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "to", undefined, MAX_DEFAULT)
                    )
                        .label(pgettext("block:scale", "To"))
                        .min(MIN)
                        .max(MAX)
                        .on(() => labelUpdate()),
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "stepSize", undefined, 1)
                    )
                        .label(pgettext("block:scale", "Step size"))
                        .min(1)
                        .max(MAX)
                        .on(() => labelUpdate()),
                ],
            },
            activated: true,
            locked: true,
            disabled: this.mode === "options",
            visible: this.mode === "numeric",
        });

        const optionsScale = this.editor.option({
            name: pgettext("block:scale", "Scale options"),
            collection: {
                collection: this.options,
                title: pgettext("block:scale", "Scale options"),
                placeholder: pgettext("block:scale", "Unnamed option"),
                showAliases: true,
                allowVariables: true,
                allowImport: true,
                allowExport: true,
                allowDedupe: true,
                autoOpen: true,
                sorting: "manual",
                onAdd: () => labelUpdate(),
                onDelete: () => labelUpdate(),
                onRename: () => labelUpdate(),
                emptyMessage: pgettext(
                    "block:scale",
                    "Click the + button to add an option..."
                ),
            },
            activated: true,
            locked: true,
            disabled: this.mode === "numeric",
            visible: this.mode === "options",
        });

        this.editor.option({
            name: pgettext("block:scale", "Labels"),
            form: {
                title: pgettext("block:scale", "Labels"),
                controls: [labelLeft, labelCenter, labelRight],
            },
            activated:
                isString(this.labelLeft) ||
                isString(this.labelCenter) ||
                isString(this.labelRight),
        });

        this.editor.option({
            name: pgettext("block:scale", "Width"),
            form: {
                title: pgettext("block:scale", "Width"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:scale",
                            "Justify the scale across the available width"
                        ),
                        Forms.Checkbox.bind(this, "justify", undefined, true)
                    ),
                ],
            },
            activated: isBoolean(this.justify),
        });

        this.editor.groups.options();
        this.editor.required(() => this.scaleSlot);
        this.editor.visibility();

        this.editor.scores({
            target: this,
            collection: optionsScale,
            description: pgettext(
                "block:scale",
                "Generates a score based on the selected option. Open the settings panel for each option to set the score."
            ),
        });

        this.editor.alias(() => this.scaleSlot);
        this.editor.exportable(() => this.scaleSlot);
    }

    @conditions
    defineConditions(): void {
        if (this.mode === "options") {
            this.options.each((option: ScaleOption) => {
                if (option.name) {
                    this.conditions.template({
                        condition: ScaleMatchCondition,
                        label: option.name,
                        burst: "branch",
                        props: {
                            slot: this.scaleSlot,
                            option,
                        },
                    });
                }
            });

            this.conditions.template({
                condition: ScaleMatchCondition,
                label: pgettext("block:scale", "Unanswered"),
                icon: ICON_UNANSWERED,
                separator: true,
                props: {
                    slot: this.scaleSlot,
                },
            });
        } else {
            each(
                [
                    {
                        mode: "equal",
                        label: pgettext("block:scale", "Answer is equal to"),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:scale",
                            "Answer is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext("block:scale", "Answer is lower than"),
                    },
                    {
                        mode: "above",
                        label: pgettext("block:scale", "Answer is higher than"),
                    },
                    {
                        mode: "between",
                        label: pgettext("block:scale", "Answer is between"),
                    },
                    {
                        mode: "not-between",
                        label: pgettext("block:scale", "Answer is not between"),
                    },
                    {
                        mode: "defined",
                        label: pgettext("block:scale", "Answer is not empty"),
                    },
                    {
                        mode: "undefined",
                        label: pgettext("block:scale", "Answer is empty"),
                    },
                ],
                (condition: { mode: TMode; label: string }) => {
                    this.conditions.template({
                        condition: ScaleCompareCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: this.scaleSlot,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(
                score.label,
                ICON_SCORE,
                false,
                this.mode !== "options"
            );

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext("block:scale", "Score is equal to"),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext("block:scale", "Score is not equal to"),
                    },
                    {
                        mode: "below",
                        label: pgettext("block:scale", "Score is lower than"),
                    },
                    {
                        mode: "above",
                        label: pgettext("block:scale", "Score is higher than"),
                    },
                    {
                        mode: "between",
                        label: pgettext("block:scale", "Score is between"),
                    },
                    {
                        mode: "not-between",
                        label: pgettext("block:scale", "Score is not between"),
                    },
                    {
                        mode: "defined",
                        label: pgettext("block:scale", "Score is calculated"),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:scale",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
